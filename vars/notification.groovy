def emailNotification() {
   //this will go in email subject section
   def subject = " Job Executed '${env.JOB_NAME} [${env.BUILD_NUMBER}]'"
   //this will go in the email body section

   def details = """<p>STARTED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
    <p>Check console output at &QUOT;<a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a>&QUOT;</p>"""
  // send to email
  emailext (
      subject: subject,
      body: details,
      to: ivan.mmeneses28@gmail.com
    )
}
